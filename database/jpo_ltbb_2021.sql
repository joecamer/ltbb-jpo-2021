-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: jpo_ltbb_2021
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `articles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `min_price` int NOT NULL,
  `category_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Kaye Potts','Anim magna quod dese',198,552,1,'2020-05-13 21:01:25','2020-05-13 21:01:25'),(2,'Ntita Ella Priscille','Anim magna quod dese',100,25,2,'2020-05-14 15:33:19','2020-05-14 15:33:19');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avis`
--

DROP TABLE IF EXISTS `avis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avis` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `note` int NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  `article_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avis`
--

LOCK TABLES `avis` WRITE;
/*!40000 ALTER TABLE `avis` DISABLE KEYS */;
/*!40000 ALTER TABLE `avis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristics`
--

DROP TABLE IF EXISTS `caracteristics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `caracteristics` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristics`
--

LOCK TABLES `caracteristics` WRITE;
/*!40000 ALTER TABLE `caracteristics` DISABLE KEYS */;
/*!40000 ALTER TABLE `caracteristics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rayon_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Malik Moreno',1,'2020-05-13 20:58:22','2020-05-13 20:58:22'),(2,'Ella',2,'2020-05-14 15:32:33','2020-05-14 15:32:33');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_rows` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int unsigned NOT NULL,
  `field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `order` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Nom',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Mot de passe',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Token de rappel',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Créé le',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Rôle',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Nom',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Créé le',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Nom',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Créé le',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Nom d\'affichage',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Rôle',1,1,1,1,1,1,NULL,9),(22,4,'id','text','Id',1,0,0,0,0,0,'{}',1),(23,4,'source','media_picker','Image',1,1,1,1,1,1,'{}',2),(24,4,'article_id','text','Article',1,0,0,1,1,1,'{}',3),(25,4,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',4),(26,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(27,4,'photo_belongsto_article_relationship','relationship','Article',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Article\",\"table\":\"articles\",\"type\":\"belongsTo\",\"column\":\"article_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(28,5,'id','text','Id',1,0,0,0,0,0,'{}',1),(29,5,'name','text','Nom',1,1,1,1,1,1,'{}',2),(30,5,'description','text','Description',1,1,1,1,1,1,'{}',3),(31,5,'price','text','Prix',1,1,1,1,1,1,'{}',4),(32,5,'min_price','text','Prix minumum',1,0,1,1,1,1,'{}',5),(33,5,'category_id','text','Categorie',1,0,0,1,1,1,'{}',6),(34,5,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',7),(35,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(36,5,'article_hasmany_photo_relationship','relationship','Photos',0,0,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Photo\",\"table\":\"photos\",\"type\":\"hasMany\",\"column\":\"article_id\",\"key\":\"id\",\"label\":\"source\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}',9),(37,5,'article_belongsto_category_relationship','relationship','Catégorie',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}',10),(38,6,'id','text','Id',1,0,0,0,0,0,'{}',1),(39,6,'name','text','Nom',1,1,1,1,1,1,'{}',2),(40,6,'rayon_id','text','Rayon',1,0,0,1,1,1,'{}',3),(41,6,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',4),(42,6,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(43,6,'category_belongsto_rayon_relationship','relationship','Rayon',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Rayon\",\"table\":\"rayons\",\"type\":\"belongsTo\",\"column\":\"rayon_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(44,7,'id','text','Id',1,0,0,0,0,0,'{}',1),(45,7,'name','text','Nom',0,1,1,1,1,1,'{}',2),(46,7,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',3),(47,7,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(48,8,'id','text','Id',1,0,0,0,0,0,'{}',1),(49,8,'session_id','text','Session Id',1,0,0,0,0,0,'{}',2),(50,8,'client_first_name','text','Prenom',1,1,1,1,1,1,'{}',3),(51,8,'client_last_name','text','Nom',0,1,1,1,1,1,'{}',4),(52,8,'client_mobile','text','Tel',1,1,1,1,1,1,'{}',5),(53,8,'client_email','text','Email',0,0,1,1,1,1,'{}',6),(54,8,'client_country','text','Pays',1,0,1,1,1,1,'{}',7),(55,8,'client_city','text','Ville',1,1,1,1,1,1,'{}',8),(56,8,'client_address1','text','Adresse 1',1,1,1,1,1,1,'{}',9),(57,8,'client_address2','text','Adresse 2',0,0,1,1,1,1,'{}',10),(58,8,'cash_to_pay','text','Total',1,0,1,1,1,1,'{}',11),(59,8,'transport_fees','text','Transport',0,0,1,1,1,1,'{}',12),(60,8,'payment_method','text','Méthode de paiement',1,0,1,1,1,1,'{}',13),(61,8,'payed','checkbox','Payé',0,1,1,1,1,1,'{\"on\":\"Oui\",\"off\":\"Non\"}',14),(62,8,'delivered','checkbox','Livré',0,1,1,1,1,1,'{\"on\":\"Oui\",\"off\":\"Non\"}',15),(63,8,'aborted','checkbox','Annulé',0,0,1,1,1,1,'{\"on\":\"Oui\",\"off\":\"Non\"}',16),(64,8,'deleted','checkbox','Supprimé',0,0,1,1,1,1,'{\"on\":\"Oui\",\"off\":\"Non\"}',17),(65,8,'created_at','timestamp','Date',0,1,1,0,0,0,'{}',18),(66,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',19),(67,8,'order_hasmany_line_relationship','relationship','Id des lignes',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Line\",\"table\":\"lines\",\"type\":\"hasMany\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}',20),(68,9,'id','text','Id',1,0,0,0,0,0,'{}',0),(69,9,'quantity','text','Quantité',1,1,1,1,1,1,'{}',2),(70,9,'article_id','text','Article Id',1,0,0,1,1,1,'{}',3),(71,9,'session_id','text','Session Id',1,0,0,0,0,0,'{}',4),(72,9,'price','text','Price',1,1,1,1,1,1,'{}',5),(73,9,'order_id','text','Order Id',1,0,0,0,0,0,'{}',6),(74,9,'created_at','timestamp','Date',0,1,1,1,0,1,'{}',7),(75,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(76,9,'line_belongsto_order_relationship','relationship','Commande',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Order\",\"table\":\"orders\",\"type\":\"belongsTo\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"articles\",\"pivot\":\"0\",\"taggable\":\"0\"}',9);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','Utilisateur','Utilisateurs','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2020-05-02 12:31:10','2020-05-02 12:31:10'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2020-05-02 12:31:11','2020-05-02 12:31:11'),(3,'roles','roles','Rôle','Rôles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController','',1,0,NULL,'2020-05-02 12:31:11','2020-05-02 12:31:11'),(4,'photos','photos','Photo','Photos','voyager-images','App\\Models\\Photo',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-05-13 20:31:44','2021-03-24 18:52:01'),(5,'articles','articles','Article','Articles','voyager-basket','App\\Models\\Article',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-05-13 20:39:41','2021-03-24 18:41:26'),(6,'categories','categories','Category','Categories','voyager-categories','App\\Models\\Category',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-05-13 20:51:26','2021-03-24 18:41:45'),(7,'rayons','rayons','Rayon','Rayons','voyager-pie-graph','App\\Models\\Rayon',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2020-05-13 20:55:54','2020-05-13 20:55:54'),(8,'orders','orders','Commande','Commandes','voyager-file-text','App\\Models\\Order',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-05-13 21:33:19','2021-03-25 09:03:14'),(9,'lines','lines','Line','Lines','voyager-list','App\\Models\\Line',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2020-05-13 21:38:42','2021-03-24 18:41:59');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entries` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `price` int NOT NULL,
  `article_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lines`
--

DROP TABLE IF EXISTS `lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lines` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `article_id` int NOT NULL,
  `session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `order_id` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lines`
--

LOCK TABLES `lines` WRITE;
/*!40000 ALTER TABLE `lines` DISABLE KEYS */;
INSERT INTO `lines` VALUES (1,3,1,'Y8Wpz7CG5hmRNZQFsmnJH5gqu1GmjqV7adDwKD4K',594,1,'2020-05-13 21:22:46','2020-05-13 21:22:46'),(2,4,2,'ey2YCutBYPQ9LuEWsxLYQckCjLHvy3zulLOjMR0b',400,2,'2020-05-14 15:35:37','2020-05-14 15:35:37'),(3,2,1,'ey2YCutBYPQ9LuEWsxLYQckCjLHvy3zulLOjMR0b',396,2,'2020-05-14 15:35:37','2020-05-14 15:35:37'),(4,5,1,'Cpf2vVpGXqblPuIwdqpKVyRteDJu7G88le9oiHhC',990,3,'2021-03-25 09:00:54','2021-03-25 09:00:54');
/*!40000 ALTER TABLE `lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `order` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Tableau de bord','','_self','voyager-boat',NULL,NULL,1,'2020-05-02 12:31:14','2020-05-02 12:31:14','voyager.dashboard',NULL),(2,1,'Médiathèque','','_self','voyager-images',NULL,NULL,5,'2020-05-02 12:31:14','2020-05-02 12:31:14','voyager.media.index',NULL),(3,1,'Utilisateurs','','_self','voyager-person',NULL,NULL,3,'2020-05-02 12:31:14','2020-05-02 12:31:14','voyager.users.index',NULL),(4,1,'Rôles','','_self','voyager-lock',NULL,NULL,2,'2020-05-02 12:31:14','2020-05-02 12:31:14','voyager.roles.index',NULL),(5,1,'Outils','','_self','voyager-tools',NULL,NULL,9,'2020-05-02 12:31:14','2020-05-02 12:31:14',NULL,NULL),(6,1,'Créateur de menus','','_self','voyager-list',NULL,5,10,'2020-05-02 12:31:14','2020-05-02 12:31:14','voyager.menus.index',NULL),(7,1,'Base de données','','_self','voyager-data',NULL,5,11,'2020-05-02 12:31:15','2020-05-02 12:31:15','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2020-05-02 12:31:15','2020-05-02 12:31:15','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2020-05-02 12:31:15','2020-05-02 12:31:15','voyager.bread.index',NULL),(10,1,'Paramètres','','_self','voyager-settings',NULL,NULL,14,'2020-05-02 12:31:15','2020-05-02 12:31:15','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,NULL,13,'2020-05-02 12:31:23','2020-05-02 12:31:23','voyager.hooks',NULL),(12,1,'Photos','','_self','voyager-images',NULL,NULL,15,'2020-05-13 20:31:45','2020-05-13 20:31:45','voyager.photos.index',NULL),(13,1,'Articles','','_self','voyager-basket',NULL,NULL,16,'2020-05-13 20:39:41','2020-05-13 20:39:41','voyager.articles.index',NULL),(14,1,'Categories','','_self','voyager-categories',NULL,NULL,17,'2020-05-13 20:51:26','2020-05-13 20:51:26','voyager.categories.index',NULL),(15,1,'Rayons','','_self','voyager-pie-graph',NULL,NULL,18,'2020-05-13 20:55:54','2020-05-13 20:55:54','voyager.rayons.index',NULL),(16,1,'Commandes','','_self','voyager-file-text',NULL,NULL,19,'2020-05-13 21:33:20','2020-05-13 21:33:20','voyager.orders.index',NULL),(17,1,'Lines','','_self','voyager-list',NULL,NULL,20,'2020-05-13 21:38:43','2020-05-13 21:38:43','voyager.lines.index',NULL),(22,1,'Menu Builder','','_self','voyager-list',NULL,21,10,'2021-03-24 16:01:56','2021-03-24 16:01:56','voyager.menus.index',NULL),(23,1,'Database','','_self','voyager-data',NULL,21,11,'2021-03-24 16:01:56','2021-03-24 16:01:56','voyager.database.index',NULL),(25,2,'Commandes','/admin/orders','_self','voyager-paper-plane','#000000',NULL,21,'2021-03-25 09:07:42','2021-03-25 09:07:42',NULL,''),(26,2,'Articles','/admin/articles','_self','voyager-bag','#000000',NULL,22,'2021-03-25 09:08:37','2021-03-25 09:08:37',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2020-05-02 12:31:14','2020-05-02 12:31:14'),(2,'gerant','2020-05-13 19:47:17','2020-05-13 19:47:17');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (35,'2014_10_12_000000_create_users_table',1),(36,'2014_10_12_100000_create_password_resets_table',1),(37,'2016_01_01_000000_add_voyager_user_fields',1),(38,'2016_01_01_000000_create_data_types_table',1),(39,'2016_05_19_173453_create_menu_table',1),(40,'2016_10_21_190000_create_roles_table',1),(41,'2016_10_21_190000_create_settings_table',1),(42,'2016_11_30_135954_create_permission_table',1),(43,'2016_11_30_141208_create_permission_role_table',1),(44,'2016_12_26_201236_data_types__add__server_side',1),(45,'2017_01_13_000000_add_route_to_menu_items_table',1),(46,'2017_01_14_005015_create_translations_table',1),(47,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(48,'2017_03_06_000000_add_controller_to_data_types_table',1),(49,'2017_04_21_000000_add_order_to_data_rows_table',1),(50,'2017_07_05_210000_add_policyname_to_data_types_table',1),(51,'2017_08_05_000000_add_group_to_settings_table',1),(52,'2017_11_26_013050_add_user_role_relationship',1),(53,'2017_11_26_015000_create_user_roles_table',1),(54,'2018_03_11_000000_add_user_settings',1),(55,'2018_03_14_000000_add_details_to_data_types_table',1),(56,'2018_03_16_000000_make_settings_value_nullable',1),(57,'2019_08_19_000000_create_failed_jobs_table',1),(58,'2020_04_14_104728_create_articles_table',1),(59,'2020_04_14_104953_create_avis_table',1),(60,'2020_04_14_105231_create_caracteristics_table',1),(61,'2020_04_14_105350_create_categories_table',1),(62,'2020_04_14_105622_create_orders_table',1),(63,'2020_04_14_105750_create_entries_table',1),(64,'2020_04_14_110022_create_lines_table',1),(65,'2020_04_14_110424_create_photos_table',1),(66,'2020_04_14_111202_create_promotions_table',1),(67,'2020_04_14_111845_create_outs_table',1),(68,'2020_04_14_113435_create_stocks_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_address1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_address2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cash_to_pay` int NOT NULL,
  `transport_fees` int DEFAULT '0',
  `payment_method` int NOT NULL,
  `payed` int DEFAULT '0',
  `delivered` int DEFAULT '0',
  `aborted` int DEFAULT '0',
  `deleted` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_session_id_index` (`session_id`),
  KEY `orders_client_mobile_index` (`client_mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (2,'ey2YCutBYPQ9LuEWsxLYQckCjLHvy3zulLOjMR0b','Sidik','FAHA','690378255','joefaha1@gmail.com','cm','cm','Akwa',NULL,796,0,0,1,1,0,0,'2020-05-14 15:35:37','2020-05-14 15:36:25'),(3,'Cpf2vVpGXqblPuIwdqpKVyRteDJu7G88le9oiHhC','Sidik','FAHA','690378255','joefaha1@gmail.com','cm','cm','Akwa Douala',NULL,990,0,0,0,0,0,0,'2021-03-25 09:00:53','2021-03-25 09:00:53');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outs`
--

DROP TABLE IF EXISTS `outs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `outs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `price` int NOT NULL,
  `article_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outs`
--

LOCK TABLES `outs` WRITE;
/*!40000 ALTER TABLE `outs` DISABLE KEYS */;
/*!40000 ALTER TABLE `outs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,3),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(27,3),(28,1),(28,3),(29,1),(29,3),(30,1),(30,3),(31,1),(31,3),(32,1),(32,3),(33,1),(33,3),(34,1),(34,3),(35,1),(35,3),(36,1),(36,3),(37,1),(37,3),(38,1),(38,3),(39,1),(39,3),(40,1),(40,3),(41,1),(41,3),(42,1),(42,3),(43,1),(43,3),(44,1),(44,3),(45,1),(45,3),(46,1),(46,3),(47,1),(47,3),(48,1),(48,3),(49,1),(49,3),(50,1),(50,3),(51,1),(51,3),(52,1),(52,3),(53,1),(53,3),(54,1),(54,3),(55,1),(55,3),(56,1),(56,3);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2020-05-02 12:31:15','2020-05-02 12:31:15'),(2,'browse_bread',NULL,'2020-05-02 12:31:15','2020-05-02 12:31:15'),(3,'browse_database',NULL,'2020-05-02 12:31:15','2020-05-02 12:31:15'),(4,'browse_media',NULL,'2020-05-02 12:31:16','2020-05-02 12:31:16'),(5,'browse_compass',NULL,'2020-05-02 12:31:16','2020-05-02 12:31:16'),(6,'browse_menus','menus','2020-05-02 12:31:16','2020-05-02 12:31:16'),(7,'read_menus','menus','2020-05-02 12:31:16','2020-05-02 12:31:16'),(8,'edit_menus','menus','2020-05-02 12:31:16','2020-05-02 12:31:16'),(9,'add_menus','menus','2020-05-02 12:31:16','2020-05-02 12:31:16'),(10,'delete_menus','menus','2020-05-02 12:31:16','2020-05-02 12:31:16'),(11,'browse_roles','roles','2020-05-02 12:31:16','2020-05-02 12:31:16'),(12,'read_roles','roles','2020-05-02 12:31:16','2020-05-02 12:31:16'),(13,'edit_roles','roles','2020-05-02 12:31:17','2020-05-02 12:31:17'),(14,'add_roles','roles','2020-05-02 12:31:17','2020-05-02 12:31:17'),(15,'delete_roles','roles','2020-05-02 12:31:17','2020-05-02 12:31:17'),(16,'browse_users','users','2020-05-02 12:31:17','2020-05-02 12:31:17'),(17,'read_users','users','2020-05-02 12:31:17','2020-05-02 12:31:17'),(18,'edit_users','users','2020-05-02 12:31:17','2020-05-02 12:31:17'),(19,'add_users','users','2020-05-02 12:31:17','2020-05-02 12:31:17'),(20,'delete_users','users','2020-05-02 12:31:17','2020-05-02 12:31:17'),(21,'browse_settings','settings','2020-05-02 12:31:17','2020-05-02 12:31:17'),(22,'read_settings','settings','2020-05-02 12:31:18','2020-05-02 12:31:18'),(23,'edit_settings','settings','2020-05-02 12:31:18','2020-05-02 12:31:18'),(24,'add_settings','settings','2020-05-02 12:31:18','2020-05-02 12:31:18'),(25,'delete_settings','settings','2020-05-02 12:31:18','2020-05-02 12:31:18'),(26,'browse_hooks',NULL,'2020-05-02 12:31:23','2020-05-02 12:31:23'),(27,'browse_photos','photos','2020-05-13 20:31:45','2020-05-13 20:31:45'),(28,'read_photos','photos','2020-05-13 20:31:45','2020-05-13 20:31:45'),(29,'edit_photos','photos','2020-05-13 20:31:45','2020-05-13 20:31:45'),(30,'add_photos','photos','2020-05-13 20:31:45','2020-05-13 20:31:45'),(31,'delete_photos','photos','2020-05-13 20:31:45','2020-05-13 20:31:45'),(32,'browse_articles','articles','2020-05-13 20:39:41','2020-05-13 20:39:41'),(33,'read_articles','articles','2020-05-13 20:39:41','2020-05-13 20:39:41'),(34,'edit_articles','articles','2020-05-13 20:39:41','2020-05-13 20:39:41'),(35,'add_articles','articles','2020-05-13 20:39:41','2020-05-13 20:39:41'),(36,'delete_articles','articles','2020-05-13 20:39:41','2020-05-13 20:39:41'),(37,'browse_categories','categories','2020-05-13 20:51:26','2020-05-13 20:51:26'),(38,'read_categories','categories','2020-05-13 20:51:26','2020-05-13 20:51:26'),(39,'edit_categories','categories','2020-05-13 20:51:26','2020-05-13 20:51:26'),(40,'add_categories','categories','2020-05-13 20:51:26','2020-05-13 20:51:26'),(41,'delete_categories','categories','2020-05-13 20:51:26','2020-05-13 20:51:26'),(42,'browse_rayons','rayons','2020-05-13 20:55:54','2020-05-13 20:55:54'),(43,'read_rayons','rayons','2020-05-13 20:55:54','2020-05-13 20:55:54'),(44,'edit_rayons','rayons','2020-05-13 20:55:54','2020-05-13 20:55:54'),(45,'add_rayons','rayons','2020-05-13 20:55:54','2020-05-13 20:55:54'),(46,'delete_rayons','rayons','2020-05-13 20:55:54','2020-05-13 20:55:54'),(47,'browse_orders','orders','2020-05-13 21:33:20','2020-05-13 21:33:20'),(48,'read_orders','orders','2020-05-13 21:33:20','2020-05-13 21:33:20'),(49,'edit_orders','orders','2020-05-13 21:33:20','2020-05-13 21:33:20'),(50,'add_orders','orders','2020-05-13 21:33:20','2020-05-13 21:33:20'),(51,'delete_orders','orders','2020-05-13 21:33:20','2020-05-13 21:33:20'),(52,'browse_lines','lines','2020-05-13 21:38:43','2020-05-13 21:38:43'),(53,'read_lines','lines','2020-05-13 21:38:43','2020-05-13 21:38:43'),(54,'edit_lines','lines','2020-05-13 21:38:43','2020-05-13 21:38:43'),(55,'add_lines','lines','2020-05-13 21:38:43','2020-05-13 21:38:43'),(56,'delete_lines','lines','2020-05-13 21:38:43','2020-05-13 21:38:43');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `photos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,'photos/dev-dots.png',1,'2020-05-13 21:13:39','2021-03-24 18:52:47'),(2,'photos/2017-10-10-17_32_04-Greenshot.png',2,'2020-05-14 15:34:00','2021-03-24 18:52:19');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promotions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int NOT NULL,
  `percentage` int NOT NULL,
  `begin_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotions`
--

LOCK TABLES `promotions` WRITE;
/*!40000 ALTER TABLE `promotions` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rayons`
--

DROP TABLE IF EXISTS `rayons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rayons` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rayons`
--

LOCK TABLES `rayons` WRITE;
/*!40000 ALTER TABLE `rayons` DISABLE KEYS */;
INSERT INTO `rayons` VALUES (1,'Valentine','2020-05-13 20:56:32','2020-05-13 20:56:32'),(2,'Ntita','2020-05-14 15:32:02','2020-05-14 15:32:02');
/*!40000 ALTER TABLE `rayons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrateur','2020-05-02 12:31:15','2020-05-02 12:31:15'),(3,'gerant','Modérateur','2020-05-13 21:44:24','2020-05-13 21:44:24'),(4,'user','Normal User','2021-03-24 16:01:56','2021-03-24 16:01:56');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NOT NULL DEFAULT '1',
  `group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Title du site','Title du site','','text',1,'Site'),(2,'site.description','Description du site','Description du site','','text',2,'Site'),(3,'site.logo','Logo du site','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics ID de Tracking',NULL,'','text',4,'Site'),(5,'admin.bg_image','Image de fond de l\'espace admin','','','image',5,'Admin'),(6,'admin.title','Titre de l\'espace admin','IGMarket Admin','','text',1,'Admin'),(7,'admin.description','Description de l\'espace admin','Panneau d\'administration de IGMarket','','text',2,'Admin'),(8,'admin.loader','Chargement de l\'espace admin','','','image',3,'Admin'),(9,'admin.icon_image','Icône de l\'espace admin','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics ID Client (Utilisé pour le panneau d\'administration)',NULL,'','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stocks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int NOT NULL,
  `quantity` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocks`
--

LOCK TABLES `stocks` WRITE;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int unsigned NOT NULL,
  `locale` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `user_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint unsigned DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i1` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Joe','joefaha1@gmail.com','users/default.png',NULL,'$2y$10$Zlx0dkrAWI3i5gJ/LOnbcu77Q3MdIDJVlWXGSMaKT63P9qnLlkNf6','QqrbVQO26wv8K5IYg5GxwW1NLKWEmQ79GsiypNYfDw0nQnGWHRx2fwjo6p72',NULL,'2020-05-02 12:31:43','2020-05-02 12:31:44'),(2,3,'Ntita','ntita.ella@igmarket.com','users/default.png',NULL,'$2y$10$uMKkCyFqffrbzSfibTcoG.4vnxoXORA.KL9VXGEHd/Vmr1h7oucJ.',NULL,'{\"locale\":\"fr\"}','2020-05-13 21:46:02','2020-05-13 21:46:02'),(3,1,'Admin','admin@localhost','users/default.png',NULL,'$2y$10$NB7CXu8qbtEwdGbBv6KNkuozhcqCEEMdZiP0ETEZkEJC35PwKBbFO','JTW847Hex3YXSqhX4LTxOgo9BypDNoc4NBuCIPb7rej5tEiOnuSeOCL8dFlo',NULL,'2021-03-24 16:04:07','2021-03-24 16:04:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-25 12:55:42
