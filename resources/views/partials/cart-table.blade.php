<h5 class="font-weight-bold h5">Articles dans le panier</h5>
@if ($data['count'] > 0)
    <table class="table table-borderless table-bordered rounded table-striped mw-100">
        <tr class="bg-dark text-white ">
            <th>#</th>
            <th>Designation</th>
            <th class="text-center">Qte</th>
            <th>Montant</th>
            <th></th>
        </tr>

        @foreach($lines as $line)
            <tr>
                <td>art-{{ $line->article_id }}</td>
                <td>{{ $line->article->name }}</td>
                <td><input class="line-quantity border-0 form-control text-center bg-transparent"
                           data-article="{{ $line->article->id }}" data-price="{{ $line->article->price }}" min="1"
                           type="number"
                           value="{{ $line->quantity }}"></td>
                <td>{{ number_format($line->price) }} Fcfa</td>
                <td>
                    <a role="button" href="/line/remove/{{ $line->article_id }}" class="btn btn-outline-danger">
                        <i class="fa fa-trash-alt"></i>
                    </a> </td>
            </tr>
        @endforeach
    </table>
@else
    <div class="row">
        <div class="col-12 alert alert-info">
            Il n'ya encore aucun produit dans votre panier !
        </div>
    </div>
@endif
