<div class="col-md-6 col-lg-3 col-sm-4 col-6 mb-3" id="article-{{ $article->id }}">
    <a href="/details/{{ $article->id }}" class="card">
        <img class="card-img-top" src="{{ asset('storage') . '/' . $article->photo }}" alt="Alt">
        <div class="card-body">
            <h6 class="card-title">{{ $article->name }}</h6>
            <p class="rating">
                <span class="stars">
                    <i class="fa fa-star"></i>
                </span>
                <strong>4.5</strong>
            </p>
            <p class="text-primary">
                {{ number_format($article->price) }} Fcfa
            </p>
        </div>
    </a>
</div>
