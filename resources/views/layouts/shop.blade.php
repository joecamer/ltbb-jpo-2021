<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') || {{ env("APP_NAME") }}</title>

    <meta name="description" content="Faites le tour, sélectionnez les articles qui vous intéressent, passez votre commande et faites vous livrer dans les plus bref délais...">

    <meta name="fb:page_id" content="43929265776"/>
  <meta name="og:email" content="admin@fanemtech.cf"/>
  <meta name="og:phone_number" content="+237 6 90 37 82 55"/>

	<meta name="og:title" content="LTBB Corp"/>
  <meta name="og:type" content="e-commerce"/>
  <meta name="og:url" content="https://ltbb-corp.cf/"/>
  <meta name="og:image" content="{{ asset("storage/fanemtech.jpg.png") }}"/>
  <meta name="og:site_name" content="LTBB Corp"/>
  <meta name="og:description" content="Faites le tour, sélectionnez les articles qui vous intéressent, passez votre commande et faites vous livrer dans les plus bref délais..."/>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('stylesheet')

</head>
<body>

<header class="sticky-top super">

    <nav class="navbar navbar-light bg-light navbar-expand-sm shadow">
        <a href="/" class="navbar-brand"> {{ strtolower(str_replace(' ', '-', env("APP_NAME"))) }}<strong>.cf</strong></a>

        <div class="collapse navbar-collapse" id="menu">

            <ul class="ml-auto navbar-nav">

                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link">
                        Accueil
                    </a>
                </li>

            </ul>
        </div>

        <ul class="ml-auto pr-4 navbar-nav">

            <li class="nav-item">
                <a href="{{ route('cart') }}" class="nav-link">
                    <i class="fa fa-shopping-cart"></i>
                    <span id="count" class="badge badge-secondary">{{ session('line_count') }}</span>
                </a>
            </li>

        </ul>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu">
            <span class="navbar-toggler-icon"></span>
        </button>


    </nav>
</header>

<main id="app">
    @yield('content')
</main>

<footer class="footer container-fluid bg-light p-5">
    <div class="row justify-content-center">
        <div class="col-md-4 col-12 col-sm-8 text-center text-secondary">
            <div class="row justify-content-lg-between">
                <div class="col">
                    <i class="fab fa-facebook fa-3x"></i>
                </div>
                <div class="col">
                    <i class="fab fa-instagram fa-3x"></i>
                </div>
                <div class="col">
                    <i class="fab fa-whatsapp fa-3x"></i>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row pt-2">
        <p class="col-12 text-center text-dark">
            &copy; {{ env("APP_NAME") }} - {{ date('Y') }}. Made with <i class="fa fa-heart"></i> by <a target="_blank" href="https://www.fanemtech.cf">Joe Codeur</a>.
        </p>
    </div>
</footer>



<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

@yield('js')

</body>
</html>
