<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') || Boutique {{ env("APP_NAME") }}</title>

    <meta name="">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    @yield('stylesheet')

</head>
<body>

<header class="sticky-top super">

    <nav class="navbar navbar-dark bg-dark navbar-expand-sm shadow">
        <a href="{{ route('admin') }}" class="navbar-brand"> ALYBABA <strong>Dashboard</strong></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="justify-content-end collapse navbar-collapse" id="menu">

            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a href="#" class="nav-link">Deconnexion</a>
                </li>

                <li class="nav-item">
                    <a target="_blank" href="{{ route('home') }}" class="nav-link">
                        Voir le site
                        <i class="fa fa-caret-right"></i>
                    </a>
                </li>

            </ul>

        </div>

    </nav>
</header>

<main id="app">
    <div class="container-fluid py-0">
        <div class="row min-vh-100">

            <!-- Left menu -->
            <div class="col-md-3 bg-dark text-light">
                <div class="row">
                    <img style="opacity: .3;" src="/public/img/slider/0.jpg" alt="Image" class="w-100">
                </div>

                <div class="row navbar navbar-dark p-0 admin-left-menu">

                    <b class="navbar-text py-3 px-3 bg-secondary w-100">Menu</b>

                    <ul class="navbar-nav w-100">

                        <li class="nav-item">
                            <a href="{{ route('new') }}" class="nav-link">
                                <i class="fa fa-plus-circle"></i>
                                Ajouter un produit
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fa fa-pen"></i>
                                Modifier un produit
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fa fa-pen"></i>
                                Gérer les catégories
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fa fa-pen"></i>
                                Ajouter une promotion
                            </a>
                        </li>

                    </ul>
                </div>

            </div>

            <!-- Sub-body -->
            <div class="py-5 col-md-9 bg-light">
                @yield('content')
            </div>
        </div>
    </div>
</main>

<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/bootstrap.min.js"></script>
<script src="/public/js/admin.js"></script>

@yield('js')

</body>
</html>
