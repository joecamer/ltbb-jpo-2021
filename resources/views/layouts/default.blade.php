<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#404040">
    <title>@yield('title_prefix') ||  </title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @yield('stylesheet')
</head>
<body>

<header class="sticky-top super">

    <nav class="navbar navbar-expand-md navbar-dark bg-primary">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>



            <div class="collapse navbar-collapse" id="search-form">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav m-auto w-75">
                    <li class="nav-item w-100">
                        <form class="input-group " action="{{ route('search') }}" method="get">
                            <input type="search" name="query" class="form-control">
                            <button class="btn btn-secondary input-group-append">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </li>
                </ul>
            </div>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a href="/cart" class="nav-link">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="badge badge-dark"> {{ session()->get('lines')['count'] }} </span>
                    </a>
                </li>

            </ul>

        </div>
    </nav>

    <nav class="navbar navbar-expand-md d-md-none bg-primary navbar-dark">
        <ul class="navbar-nav w-100">
            <li class="nav-item">
                <form action="{{ route('search') }}" method="get" class="input-group">
                    <input type="search" name="query" class="form-control">
                    <button class="btn btn-secondary input-group-append">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
            </li>
        </ul>
    </nav>

</header>

<main>
    @yield('content')
</main>

<footer class="container-fluid p-md-5">

    <div class="row">

        <div class="col-md-4 col-12">
            <h3>ALYBABA <strong style="color: #AA3030; text-shadow: 0 0 1px #fff;">SARL</strong></h3>
            <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque sint similique
            </p>
        </div>

        <div class="col-md-4 col-12 contacts">
            <h3>Contacts</h3>
            <ul>
                <li>
                    <i class="fa fa-phone"></i>
                    6 90 90 90 90 / 6 70 70 70 70
                </li>

                <li>
                    <i class="fa fa-envelope"></i>
                    alybaba05@yahoo.com
                </li>

                <li>
                    <i class="fa fa-map-marker" ></i>
                    AKWA, face Lycée technique d'Akwa
                </li>
            </ul>
        </div>

        <div class="col-md-4">

            <div class="row follow">
                <div class="col-12">
                    <h3>Suivez nous</h3>
                    <ul>

                        <li>
                            <i class="fab fa-facebook-f"></i>
                        </li>

                        <li>
                            <i class="fab fa-twitter"></i>
                        </li>

                        <li>
                            <i class="fab fa-whatsapp"></i>
                        </li>

                        <li>
                            <i class="fab fa-instagram"></i>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <h3><label for="newslater">Soyez toujours à jour</label></h3>
                    <div class="col-12">

                        <form action="#" method="post" class="input-group w-60">
                            <input
                                class="form-control"
                                type="email"
                                name="newslater"
                                id="newslater"
                                placeholder="Email..."
                            >

                            <button class="btn bg-primary input-group-append">
                                Envoyer
                            </button>

                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <p>
                &copy; ALYBABA SARL {{ date('Y') }}. Made with <i class="fa fa-heart"></i> By <a href="http://fb.com/geekjoeofficiel">Joe Codeur</a>
            </p>
        </div>
    </div>

</footer>

<script src="{{ asset('public/js/jquery.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/main.js') }}"></script>
@yield('js')
</body>
</html>
