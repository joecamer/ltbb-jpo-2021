@extends('layouts.default')

@section('title_prefix')
    Page d'accueil
@endsection

@section('content')
    <div id="home" data-target="#spyzone" data-offset="0">
        <!-- Slider -->
        <article class="container-fluid slide">
            <div class="row">
                <div class="slider">

                    <div class="col-12 slider-content slider-active">
                        <img src="/public/img/slider/0.jpg" alt="Slider 0">
                    </div>

                    <div class="slider-content">
                        <img src="/public/img/slider/1.jpg" alt="Slider 0">
                    </div>

                    <div class="slider-content">
                        <img src="/public/img/slider/2.jpg" alt="Slider 0">
                    </div>

                    <ul class="slider-count">

                    </ul>
                </div>
            </div>
        </article>

        <!-- About -->
        <div id="about" class="jumbotron">
            <h1>Qui sommes nous ?</h1>
            <div class="row">
                <div class="text-center p-2 p-md-5 p-lg-5 col-12">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet asperiores distinctio dolor earum esse ipsam laudantium, perferendis possimus qui soluta suscipit tempora voluptatem voluptatibus? Asperiores cupiditate minima nemo quam tenetur.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum delectus esse fuga in ipsum itaque nisi nobis omnis perspiciatis quis quisquam quos reprehenderit rerum saepe sint, sit, suscipit voluptatem, voluptatibus?
                    </p>
                </div>
            </div>
        </div>

        <!-- Secteurs d'activité -->
        <div id="activites" class="container sec">

            <div class="row">
                <div class="col-12">
                    <h1>Nos secteurs d'activité</h1>
                </div>
            </div>

            @for ($i = 0; $i < 4; $i++)
                @if($i % 2 === 0)
                    <div class="row align-items-center py-5">

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-12 text-center d-flex justify-content-center px-5 align-items-center flex-column">
                                    <h2 class="text-primary">
                                        <strong>Secteur N° {{ $i + 1 }}</strong>
                                    </h2>
                                    <p style="font-size: 1.3em;" class="text-muted">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus quasi quia voluptatem...
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 p-0">
                            <img src="/public/img/slider/0.jpg" alt="Test">
                        </div>

                    </div>
                @else
                    <div class="row align-items-center py-5">

                        <div class="col-md-6 p-0">
                            <img src="/public/img/slider/0.jpg" alt="Test">
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-12 text-center d-flex justify-content-center px-5 align-items-center flex-column">
                                    <h2 class="text-primary">
                                        <strong>Secteur N° {{ $i + 1 }}</strong>
                                    </h2>
                                    <p style="font-size: 1.3em;" class="text-muted">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus quasi quia voluptatem...
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                @endif
            @endfor

        </div>

        <!-- Services -->
        <div id="services" class="container">

            <h1>Les services d'Alybaba</h1>

            <div class="row services justify-content-center align-items-center">

                <div class="col-md-4 col-6 service">
                    <div class="circle">
                        <i class="fa fa-user"></i>
                    </div>

                    <div class="content">
                        <h4>Service quelconque</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nihil, vitae? Doloribus vel dolor dignissimos id odit voluptatum quia esse sit perspiciatis!
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-6 service">
                    <div class="circle">
                        <i class="fa fa-user"></i>
                    </div>

                    <div class="content">
                        <h4>Service quelconque</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nihil, vitae? Doloribus vel dolor dignissimos id odit voluptatum quia esse sit perspiciatis!
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-6 service">
                    <div class="circle">
                        <i class="fa fa-user"></i>
                    </div>

                    <div class="content">
                        <h4>Service quelconque</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nihil, vitae? Doloribus vel dolor dignissimos id odit voluptatum quia esse sit perspiciatis!
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-6 service">
                    <div class="circle">
                        <i class="fa fa-user"></i>
                    </div>

                    <div class="content">
                        <h4>Service quelconque</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nihil, vitae? Doloribus vel dolor dignissimos id odit voluptatum quia esse sit perspiciatis!
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-6 service">
                    <div class="circle">
                        <i class="fa fa-user"></i>
                    </div>

                    <div class="content">
                        <h4>Service quelconque</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nihil, vitae? Doloribus vel dolor dignissimos id odit voluptatum quia esse sit perspiciatis!
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-6 service">
                    <div class="circle">
                        <i class="fa fa-user"></i>
                    </div>

                    <div class="content">
                        <h4>Service quelconque</h4>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nihil, vitae? Doloribus vel dolor dignissimos id odit voluptatum quia esse sit perspiciatis!
                        </p>
                    </div>
                </div>

            </div>

        </div>

        <!-- Equipe -->
        <div id="team" class="container">

            <h1>Notre équipe</h1>

            <div class="row team py-5 justify-content-around">

                <div class="col-md-4 col-2 justify-content-center">

                    <div class="team-item">

                        <div class="team-profile">
                            <img src="{{ asset('public/img/slider/0.jpg') }}" alt="Equipier">
                        </div>

                        <hr>

                        <div class="team-bio">
                            <h5><strong>Son nom</strong></h5>
                            <i class="poste">
                                Son poste
                            </i>
                        </div>

                        <ul class="team-links">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-google-plus-g" ></i>
                                </a>
                            </li>
                        </ul>

                    </div>

                </div>

                <div class="col-md-4 col-2 justify-content-center">

                    <div class="team-item">

                        <div class="team-profile">
                            <img src="{{ asset('public/img/slider/0.jpg') }}" alt="Equipier">
                        </div>

                        <hr>

                        <div class="team-bio">
                            <h5><strong>Son nom</strong></h5>
                            <i class="poste">
                                Son poste
                            </i>
                        </div>

                        <ul class="team-links">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-google-plus-g" ></i>
                                </a>
                            </li>
                        </ul>

                    </div>

                </div>

                <div class="col-md-4 col-2 justify-content-center">

                    <div class="team-item">

                        <div class="team-profile">
                            <img src="{{ asset('public/img/slider/0.jpg') }}" alt="Equipier">
                        </div>

                        <hr>

                        <div class="team-bio">
                            <h5><strong>Son nom</strong></h5>
                            <i class="poste">
                                Son poste
                            </i>
                        </div>

                        <ul class="team-links">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-google-plus-g" ></i>
                                </a>
                            </li>
                        </ul>

                    </div>

                </div>

            </div>

        </div>

        <!-- Témoignages -->
        <div class="container coms py-5">

            <h1>Témoignages de nos clients</h1>

            <div class="row justify-content-around _coms">

                <div class="col-md-5 com">

                    <div class="profile">
                        <img src="{{ asset('public/img/slider/0.jpg') }}" alt="Profile">
                    </div>

                    <div class="content">
                        <h3>Username</h3>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magnam, quaerat!
                        </p>
                    </div>

                </div>


                <div class="col-md-5 com">

                    <div class="profile">
                        <img src="{{ asset('public/img/slider/0.jpg') }}" alt="Profile">
                    </div>

                    <div class="content">
                        <h3>Username</h3>
                        <p>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magnam, quaerat! Assumenda unde
                        </p>
                    </div>

                </div>

            </div>
        </div>

        <!-- Album -->
        <div id="album" class="container album">
            <h1>Albums</h1>
            <div class="row portfolio">
                @for ($i = 0; $i < 12; $i++)
                   <div class="col-md-3">
                       <img src="{{ asset('public/img/slider/0.jpg') }}" alt="Photo" class="w-100">
                   </div>
                @endfor
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public/js/slider.js') }}"></script>
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="/public/css/slider.css">
@endsection
