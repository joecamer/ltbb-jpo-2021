@extends('layouts.admin')

@section('title')
    Panneau de controle
@endsection

@section('content')
    <div class="row">

        <div class="col-12">
            <h3 class="text-primary font-weight-bold text-center py-4">Vue d'ensemble</h3>

            <div class="row">
                <div class="col">
                    <div class="py-4 text-center round border shadow bg-danger">
                        <div class="row">
                            <div class="col-12 font-weight-bold text-big">{{ $orders }}</div>
                        </div>
                        <div class="row">
                            <div class="col-12">Commandes en attente</div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="py-4 text-center round border shadow bg-info">
                        <div class="row">
                            <div class="col-12 font-weight-bold text-big">{{ $comments }}</div>
                        </div>
                        <div class="row">
                            <div class="col-12">Nouvaux commentaires</div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="py-4 text-center round border shadow bg-primary">
                        <div class="row">
                            <div class="col-12 font-weight-bold text-big">{{ $in }}</div>
                        </div>
                        <div class="row">
                            <div class="col-12">Nouvelles entrées</div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="py-4 text-center round border shadow bg-dark">
                        <div class="row">
                            <div class="col-12 font-weight-bold text-big">{{ $outs }}</div>
                        </div>
                        <div class="row">
                            <div class="col-12">Nouvelles sorties</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <h3 class="text-primary font-weight-bold text-center py-4">Les 25 derniers produits ajoutés</h3>

            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-dark text-light">
                            <th>Désigation</th>
                            <th>Prix</th>
                            <th>Prix Min</th>
                        </tr>

                        @foreach ($articles as $article)
                            <tr>
                                <td>{{ $article->name }}</td>
                                <td>{{ number_format($article->price) }} Fcfa</td>
                                <td>{{ number_format($article->min_price) }} Fcfa</td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
