@extends('layouts.shop')

@section('title')
    Confirmez votre commande
@endsection

@section('content')
    <div class="container-fluid bg-dark text-light">
        <div class="row">
            <div class="col-12 py-5 text-center">
                <h1>Confirmez votre commande</h1>
            </div>
        </div>
    </div>
    <form data-token="{{ csrf_token() }}" id="checkoutForm" method="post" class="container py-5 position-relative">

        @csrf

        <div class="loader-content">
            <div class="circle">
                <div class="loader">
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <h2>Vos informations</h2>
                <hr>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="first">Votre nom</label>
                        <input type="text" name="first" id="first" class="form-control" required>
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="last">Votre prenom</label>
                        <input type="text" name="last" id="last" class="form-control">
                    </div>

                    <div class="col-md-12 form-group">
                        <label for="email">Votre email</label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>

                    <div class="col-md-12 form-group">
                        <label for="tel">Votre téléphone</label>
                        <input type="tel" name="tel" id="tel" class="form-control" required>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="country">Votre pays</label>
                        <select class="form-control" name="country" id="country" required>
                            <option value="cm">Cameroun</option>
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="city">Votre Ville</label>
                        <select class="form-control" name="city" id="city" required>
                            <option value="cm">Maroua</option>
                            <option value="cm">Garoua</option>
                            <option value="cm">Ngaoundéré</option>
                            <option value="cm">Yaoundé</option>
                            <option value="cm">Douala</option>
                            <option value="cm">Bafoussam</option>
                            <option value="cm">Bamenda</option>
                            <option value="cm">Buea</option>
                            <option value="cm">Bertoua</option>
                            <option value="cm">Ebolowa</option>
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="box">Votre B.P</label>
                        <input type="text" name="box" id="box" class="form-control">
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="addr1">Adresse N°1</label>
                        <input type="text" name="addr1" id="addr1" class="form-control" required>
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="addr2">Adresse N°2</label>
                        <input type="text" name="addr2" id="addr2" class="form-control">
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <h2>Paiement</h2>

                <hr>

                <div class="row">
                    <div class="col-12 d-flex flex-column justify-content-between">

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="method">Moyen de paiement</label>
                                <select id="method" name="method" class="form-control" required>
                                    <option value="0">Espèce</option>
                                </select>
                            </div>

                            <hr>

                            <div class="col-12">
                                @include('partials.checkout-lines', compact(['lines', 'data']))
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <p>Montant total : <b>{{ $data['sum'] }} Fcfa</b></p>
                                <p>Frais de transport : <b>0 Fcfa</b></p>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-12 d-flex justify-content-end">
                <button type="submit" class="btn btn-outline-success">
                    Commander
                </button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        $(_ => {
            $('.loader-content').fadeOut(200)
            App.checkoutInit()
        })
    </script>
@endsection
