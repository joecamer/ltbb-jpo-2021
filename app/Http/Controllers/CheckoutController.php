<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    public function index()
    {
        $lines = Session::get('lines');
        $sum = 0;
        $total = 0;

        foreach ($lines as $line) {
            $sum += $line->price;
            $total += $line->quantity;
        }

        $data = [
            'sum' => $sum,
            'total' => $total,
            'count' => Session::get('line_count')
        ];

        return view('shop.checkout', compact(['lines', 'data']));
    }
}
