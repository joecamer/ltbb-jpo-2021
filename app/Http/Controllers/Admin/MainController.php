<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Avis;
use App\Entry;
use App\Http\Controllers\Controller;
use App\Order;
use App\Out;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $articles = Article::query()->limit(25)->orderBy('created_at')->get();
        $orders = Order::query()->where('delivered', '=', 0)->count();
        $comments = Avis::query()->count();
        $outs = Out::query()->count();
        $in = Entry::query()->count();

        return view('admin.home', compact(['articles', 'orders', 'comments', 'outs', 'in']));
    }

    public function new_product()
    {
        return view('admin.new-article');
    }
}
