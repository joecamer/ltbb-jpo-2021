<?php

namespace App\Http\Controllers;

use App\Caracteristic;
use Illuminate\Http\Request;

class CaracteristicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Caracteristic  $caracteristic
     * @return \Illuminate\Http\Response
     */
    public function show(Caracteristic $caracteristic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Caracteristic  $caracteristic
     * @return \Illuminate\Http\Response
     */
    public function edit(Caracteristic $caracteristic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Caracteristic  $caracteristic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Caracteristic $caracteristic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Caracteristic  $caracteristic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Caracteristic $caracteristic)
    {
        //
    }
}
