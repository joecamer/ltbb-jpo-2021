<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Rayon;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function index(Request $request) {

        $rcat = $request->get('category');
        $rq = $request->get('query');

        if ($rq !== null ) {
            if (isset($rcat) && $rcat !== '') {
                $searchResult = Article::query()->whereIn('category_id', $rcat)->where('name', 'regexp', $rq)->paginate(env('PAGINATION_NUMBER'));
                $searchResult = $searchResult->appends('query', $rq)->appends('category', $rcat);
            } else {
                $searchResult = Article::query()->where('name', 'regexp', $rq)->paginate(env('PAGINATION_NUMBER'));
                $searchResult = $searchResult->appends('query', $rq);
            }
        } elseif ($rcat !== null) {
            $searchResult = Article::query()->whereIn('category_id', $rcat)->paginate(env('PAGINATION_NUMBER'));
            $searchResult = $searchResult->appends('category', $rcat);
        } else {
            $searchResult = Article::query()->paginate(env('PAGINATION_NUMBER'));
        }


        $href = $request->getRequestUri();
        $temp = str_replace('&ajax=1', '', $href);
        $href = $temp;
        $articles = $searchResult;

        if ($request->get('ajax'))
            return view('partials._search', compact(['articles', 'href', 'rcat']));

        $parents = Rayon::all();
        return view('shop.search', compact(['articles', 'href', 'parents' , 'rcat']));

    }

}
